/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar -D mapred.reduce.tasks=1 -input input0/green_tripdata_2016-01.csv -output out/output1 -mapper 'src/pycsv.py' -reducer aggregate

/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar -D mapred.reduce.tasks=1 -input input0/green_tripdata_2016-01.csv -input input0/green_tripdata_2016-02.csv -output out/mean-distance-by-pass-green -mapper 'src/mean-by-fields/meanByFieldMap.py 9 10' -reducer 'src/mean-by-fields/meanByFieldRed.py'

sed 's/ \+/,/g' out/mean-distance-by-pass-green/part-00000 > out/mean-distance-by-pass-green/results.csv

/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar -D mapred.reduce.tasks=1 -input input0/green_tripdata_2016-01.csv -output out/mean-totalamount-by-paymenttype-green -mapper 'src/mean-by-fields/meanByFieldMap.py 19 18' -reducer 'src/mean-by-fields/meanByFieldRed.py'

sed 's/ \+/,/g' out/mean-totalamount-by-paymenttype-green/part-00000 > out/mean-totalamount-by-paymenttype-green/results.csv

/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar -D mapred.reduce.tasks=1 -input input0/yellow_tripdata_2016-01.csv -output out/mean-distance-by-pass-yellow -mapper 'src/mean-by-fields/meanByFieldMap.py 3 4' -reducer 'src/mean-by-fields/meanByFieldRed.py'

sed 's/ \+/,/g' out/mean-distance-by-pass-yellow/part-00000 > out/mean-distance-by-pass-yellow/results.csv

/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar -D mapred.reduce.tasks=1 -input input0/yellow_tripdata_2016-01.csv -output out/mean-totalamount-by-paymenttype-yellow -mapper 'src/mean-by-fields/meanByFieldMap.py 11 18' -reducer 'src/mean-by-fields/meanByFieldRed.py'

sed 's/ \+/,/g' out/mean-totalamount-by-paymenttype-yellow/part-00000 > out/mean-totalamount-by-paymenttype-yellow/results.csv

/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar -D mapred.reduce.tasks=1 -input input0/green_tripdata_2016-01.csv -output out/summ-value-rate-code-green -mapper 'src/summ-by-value/summByValueMap.py 4' -reducer aggregate

sed 's/ \+/,/g' out/summ-value-rate-code-green/part-00000 > out/summ-value-rate-code-green/results.csv

/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar -D mapred.reduce.tasks=1 -input input0/green_tripdata_2016-01.csv -output out/dropoff-longlat-long -mapper 'src/visual/thirdFieldRelMap.py 7 8 10 1 4' 

/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar -D mapred.reduce.tasks=1 -input input0/green_tripdata_2016-01.csv -output out/dropoff-longlat-short -mapper 'src/visual/thirdFieldRelMap.py 7 8 10 0 2' 

/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar -D mapred.reduce.tasks=1 -input input0/green_tripdata_2016-01.csv -output out/pickup-longlat-short -mapper 'src/visual/thirdFieldRelMap.py 5 6 10 0 2' 

/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar -D mapred.reduce.tasks=1 -input input0/green_tripdata_2016-01.csv -output out/pickup-longlat-long -mapper 'src/visual/thirdFieldRelMap.py 5 6 10 1 4' 

sed 's/ \+/,/g' out/pickup-longlat-long/part-00000 > results1.csv
sed 's/ \+/,/g' out/pickup-longlat-short/part-00000 > results2.csv
sed 's/ \+/,/g' out/dropoff-longlat-long/part-00000 > results3.csv
sed 's/ \+/,/g' out/dropoff-longlat-short/part-00000 > results4.csv

google-chrome http://localhost
