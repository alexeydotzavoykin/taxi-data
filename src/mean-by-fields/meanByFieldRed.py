#!/usr/bin/env python
import sys
import collections

key_line = "";
value_line = "";
data_dict = collections.defaultdict(list)
def mean(data_list):
    return sum(data_list)/float(len(data_list)) if len(data_list) else 0

for line in sys.stdin:
    try:
        #line = line.rstrip(os.linesep)
	serial_id, duration = line.split(',')
	if not key_line and not serial_id.isdigit():
		key_line = serial_id;
		value_line = duration;
        else:
		data_dict[serial_id].append(float(duration))
    except Exception:
        pass
od = collections.OrderedDict(sorted(data_dict.items()))
print "%s,%s" %(key_line, value_line)
for k,v in od.items():
    print "%s,%s" %(k, mean(v))
