#!/usr/bin/env python
import sys
import collections

data_dict = collections.defaultdict(list)

for line in sys.stdin:
    try:
        #line = line.rstrip(os.linesep)
        serial_id, duration = line.split(',')
        data_dict[serial_id].append(float(duration))
    except Exception:
        pass
od = collections.OrderedDict(sorted(data_dict.items()))
for k,v in od.items():
    print "%s,%s" %(k, sum(v))
