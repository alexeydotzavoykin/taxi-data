#!/usr/bin/env python
import sys

keyField = -1;
valueField = -1;
count = 0;
fieldsCounter = 0;
for line in sys.stdin:
    try:
	    count=count+1;
	    if (valueField == -1) or (keyField == -1):
	# get numbers of fields to operate with
            	tokens = line.split(',')
            	for x in tokens:
			if (str(x).lower() == str(sys.argv[1]).lower()):
				keyField = fieldsCounter;
			if (str(x).lower() == str(sys.argv[2]).lower()):
				valueField = fieldsCounter;
			fieldsCounter = fieldsCounter+1;
	    if (count > 2) and (valueField != -1) and (keyField != -1):
	# remove leading and trailing whitespace
        #line = line.rstrip(os.linesep)
            	tokens = line.split(',')
            	print "%s,%s" % (tokens[keyField],tokens[valueField])

    except Exception:
        continue

