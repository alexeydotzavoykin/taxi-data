import org.jfree.chart.annotations.XYTextAnnotation
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.SparkConf
import breeze.linalg._
import breeze.plot._
import org.apache.spark.sql.{SparkSession, SQLContext}
import org.apache.spark.sql.functions.year
import org.apache.spark.sql.functions.hour
import java.time.OffsetDateTime
import org.apache.spark.sql.functions._

import scala.collection.mutable.ListBuffer

  object TestScala {

    def avgByTwoFieldsWithDate(frame: DataFrame, xColumn: String, yColumn: String, plt: Plot): Unit = {
          //val format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss")

          val averPrice = frame
            .select("hour", yColumn)
            .rdd
            .map(r => (r(0).toString.toInt, (r(1).toString.toDouble, 1)))
            .reduceByKey((x, y) => (x._1 + y._1, x._2 + y._2))
            .mapValues(y => 1.0 * y._1 / y._2)
            .collect

          val vectorXAxis = ListBuffer[Double]()

          val vectorYAxis = ListBuffer[Double]()

          averPrice.foreach {
            e =>
              val (c, n) = e
              vectorXAxis += c.toString.toInt
              vectorYAxis += n
          }
        plt += plot(new DenseVector(vectorXAxis.toList.toArray),
            new DenseVector(vectorYAxis.toList.toArray), '.', "red", "Chart", lines = true, shapes = true)
          plt.xlabel = xColumn
          plt.ylabel = yColumn
        }

    def avgByTwoFields(frame: DataFrame, xColumn: String, yColumn: String, plt: Plot): Unit = {

      val averPrice = frame
        .select(xColumn, yColumn)
        .rdd
        .map(r => (r(0).toString.toDouble,(r(1).toString.toDouble,1)))
        .reduceByKey((x, y) => (x._1 + y._1, x._2 + y._2))
        .mapValues(y => 1.0 * y._1 / y._2)
        .collect

      val vectorXAxis = ListBuffer[Double]()

      val vectorYAxis = ListBuffer[Double]()

      averPrice.foreach{
        e =>
          val (c, n) = e
          vectorXAxis += c.toDouble
          vectorYAxis += n
      }

    plt += plot(new DenseVector(vectorXAxis.toList.toArray),
        new DenseVector(vectorYAxis.toList.toArray), '.', "red", "Chart", lines = true, shapes = true)
      plt.xlabel = xColumn
      plt.ylabel = yColumn
    }

    def sumByTwoFields(frame: DataFrame, xColumn: String, yColumn: String, plt: Plot): Unit = {

      val averPrice = frame
        .select(xColumn, yColumn)
        .rdd
        .map(r => (r(0).toString.toDouble,(r(1).toString.toDouble,1)))
        .reduceByKey((x, y) => (x._1 + y._1, x._2 + y._2))
        .mapValues(y => 1.0 * y._1)
        .collect

      val vectorXAxis = ListBuffer[Double]()

      val vectorYAxis = ListBuffer[Double]()

      averPrice.foreach{
        e =>
          val (c, n) = e
          vectorXAxis += c.toDouble
          vectorYAxis += n
      }

      plt += plot(new DenseVector(vectorXAxis.toList.toArray),
        new DenseVector(vectorYAxis.toList.toArray), '.', "red", "Chart", lines = true, shapes = true)
      plt.xlabel = xColumn
      plt.ylabel = yColumn
    }

    def main(args: Array[String]): Unit = {
      val fig = Figure("Taxi charts")
      val plt = fig.subplot(0)
      val pltTwo = fig.subplot(2,3,1)
      val pltThree = fig.subplot(2,3,2)
      val pltFour = fig.subplot(2,3,3)
      val pltFive= fig.subplot(2,3,4)
      val pltSix= fig.subplot(2,3, 5)

      System.setProperty("hadoop.home.dir", "C:/winutils/")
      val conf = new SparkConf()
      val spark = SparkSession.builder
        .master("local[2]")
        .appName("My App")
        .config("spark.sql.warehouse.dir", "file:///C:/Users/alexey.zavoykin/Desktop/Akvelon/SparkProject")
        .config(conf)
        .getOrCreate()
      import spark.sqlContext.implicits._
      val frameTemplateGreen = spark.read.format("csv")
        .option("header","true")
        .load("green_tripdata_2016-01.csv")

      val frameTemplateYellow = spark.read.format("csv")
        .option("header","true")
        .load("yellow_tripdata_2016-01.csv")

      val cols1 = frameTemplateGreen.columns.map((x)=>x.toLowerCase()).toSet
      val cols2 = frameTemplateYellow.columns.map((x)=>x.toLowerCase()).toSet
      val total = cols1 ++ cols2 // union

      def expr(myCols: Set[String], allCols: Set[String]) = {
        allCols.toList.map(x => x match {
          case x if myCols.contains(x) => col(x)
          case _ => lit(null).as(x)
        })
      }

      val frameTemplate =  frameTemplateGreen.select(expr(cols1, total):_*)
        .union(frameTemplateYellow.select(expr(cols2, total):_*))
        .distinct()
      frameTemplate.printSchema()
      //val frameTemplate = frameTemplateGreen.union(frameTemplateYellow)

      val frame = frameTemplate.filter("RateCodeID < 7.0")
      //frame.schema.add("year", org.apache.spark.sql.types.IntegerType, nullable = true, DateTime.parse(col1, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).getYear)
      frame
        .withColumn("lpep_pickup_datetime", $"lpep_pickup_datetime".cast("timestamp"))
        .withColumn("hour", hour($"lpep_pickup_datetime"))

      frame.printSchema()


      /*
      По средствам оплаты:
      1) Средняя цена
      Для каждого средства оплаты вывести среднюю цену
      */
      avgByTwoFields(frame, "Payment_type", "Total_amount", plt)
      val paymentTypesText = "1=Credit card 2=Cash 3=No charge 4=Dispute 5=Unknown 6=Voided trip"

      /*
       2) Среднее число пассажиров для разной ценовой категории
      */
      avgByTwoFields(frame, "Payment_type", "Passenger_count",pltTwo)
      /*
      По расстоянию:
      1) Средняя цена
      */
      avgByTwoFields(frame, "Trip_distance", "Total_amount", pltThree)
      /*
      2) Среднее число пассажиров
      */
      avgByTwoFields(frame, "Trip_distance", "Passenger_count", pltFour)
      plt.plot.addAnnotation(new  XYTextAnnotation(paymentTypesText, 3.0, 10.0))

      //по времени средние данные
      //avgByTwoFieldsWithDate(frame, "lpep_pickup_datetime", "Trip_distance", pltFive)
      //
      // По rate code, средний платёж
      avgByTwoFields(frame, "RateCodeID", "Total_amount", pltFive)

      sumByTwoFields(frame, "RateCodeID", "Total_amount", pltSix)

      fig.refresh()
    }
  }